const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]);


 mix.styles([
        'resources/assets/vendor/fontawesome-free/css/all.min.css',
        'resources/assets/vendor/animate.css/animate.min.css',
        'resources/assets/vendor/bootstrap/css/bootstrap.min.css',
        'resources/assets/vendor/bootstrap-icons/bootstrap-icons.css',
        'resources/assets/vendor/boxicons/css/boxicons.min.css',
        'resources/assets/vendor/glightbox/css/glightbox.min.css',
        'resources/assets/vendor/remixicon/remixicon.css',
        'resources/assets/vendor/swiper/swiper-bundle.min.css',
        'resources/assets/css/style.css',
    ], 'public/css/app.css')
    
        .scripts([
           'resources/assets/vendor/purecounter/purecounter.js',
           'resources/assets/vendor/bootstrap/js/bootstrap.bundle.min.js',
           'resources/assets/vendor/glightbox/js/glightbox.min.js',
           'resources/assets/vendor/swiper/swiper-bundle.min.js',
           'resources/assets/vendor/php-email-form/validate.js',
           'resources/assets/js/main.js',
           ], 'public/js/app.js');
