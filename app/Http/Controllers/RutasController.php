<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RutasController extends Controller
{
    public function about(){
        return view('about.index');
    }

    public function solution(){
        return view('solution.index');
    }

    public function team(){
        return view('team.index');
    }
}
