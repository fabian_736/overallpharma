<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Inscription;

class InscriptionController extends Controller
{
    public function create(){
        return view('layouts.utils.modal');
    }

    public function store(Request $request){

        Inscription::create($request->all());
        return redirect('/')->with('create', 'Usuario Registrado');
    }


}
