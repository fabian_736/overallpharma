<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inscription extends Model
{
    use HasFactory;

    protected $table='inscriptions';
    protected $primaryKey='id';
    protected $fillable =[
        'name',
        'last_name',
        'business',
        'email',
        'phone',
        'country',
        'description'
    ];

}
