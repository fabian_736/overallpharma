<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RutasController;
use App\Http\Controllers\InscriptionController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



Route::get('/about',[RutasController::class,'about'])->name('about.index');
Route::get('/solution',[RutasController::class,'solution'])->name('solution.index');
Route::get('/team',[RutasController::class,'team'])->name('team.index');


Route::get('/modal',[InscriptionController::class,'create'])->name('modal');
Route::post('/submit',[InscriptionController::class,'store'])->name('inscription.store'); 