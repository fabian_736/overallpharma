@extends('layouts.app')
@section('content')


    <div class="col-lg-10 col-md-10 d-flex align-items-stretch mb-3 mx-auto">
        <div class="card shadow" style="border-radius: 20px">
            <div class="row-reverse my-3">
                <div class="col d-flex justify-content-center">
                    <div class="icon d-flex justify-content-center">
                        <img src="https://newlink-group.com/uploads/press/60689774c2b756af4354b8d7a4c35d29f5788abc.jpg" alt="" class="w-75">
                    </div>
                </div>
                <div class="col d-flex justify-content-center my-3">
                    <h4><a href="javascript:;">IT</a></h4>
                </div>
                <div class="col-10 d-flex justify-content-center mx-auto">
                  <ul>
                  
                        <li>Desde nuestras plataformas digitales personalizadas, arquitectura cloud e infraestructura escalable
                        hasta nuestros Dashboards BI y Analytic Insights contamos con tecnología de vanguardia que permiten
                        el mejor desempeño de nuestras operaciones y la flexibilidad necesaria que garantiza una mejora
                        constante de nuestras soluciones.</li> <br>

                        <li>Gracias a la solidez de nuestras soluciones, confiabilidad en la analítica de datos y respaldados
                        por un equipo de alto nivel en programación y ciencia de datos logramos ejecutar estrategias basadas
                        en data (data-driven strategies).</li> 
                      </ul>
                </div>
            </div>
        </div>
    </div>

@endsection
