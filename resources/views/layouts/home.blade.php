@include('layouts.components.head')
@include('layouts.components.navbar')

  <!-- ======= TITULO PRINCIPAL ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <label for="" class="h1"  style="margin-top: 10% !important; color: #2c4964; font-weight: bold">Overall Pharma</label>
      <div class="col-8 my-3">
        <div id="carouselContent" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner" role="listbox">
                <div class="carousel-item active" style="padding-right: 15%;">
                     <h3>Overall Pharma es una empresa pionera en la región latinoamericana en el desarrollo de innovadoras y efectivas soluciones operativas que potencian y facilitan la obtención de resultados de compañías que operan en el área de Salud. </h3>
                </div>
                <div class="carousel-item " style="padding-right: 15%;">
                    <h3>Contamos con plataformas digitales propias que combinadas con la probada experiencia de nuestro equipo de trabajo interdisciplinario, garantizan la excelencia ejecucional de cada proyecto que nos es confiado. </h3>
                </div>
                <div class="carousel-item " style="padding-right: 15%;">
                    <h3>Nos inspira, motiva y apasiona trabajar de la mano de nuestros clientes soluciones que maximicen la efectividad de los recursos invertidos en el área de salud, para mejorar la calidad de vida de los pacientes, garantizar la educación médica continuada y facilitar el acceso a tecnologías farmacéuticas.</h3>
                </div>
            </div>
            <a class="carousel-control-next" href="#carouselContent" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
      <a href="javascript:;" class="btn scrollto text-white col-3 p-2" data-toggle="modal" data-target="#exampleModalCenter" style="background: #197FA8; border-radius: 20px">Contáctenos</a>
    </div>


  </section>

  
    <!-- ======= FIN TITULO PRINCIPAL ======= -->

  <main id="main">

      <!-- ======= MODAL FORMULARIO ======= -->
      @include('layouts.utils.modal')
      <!-- ======= FIN MODAL FORMULARIO ======= -->


    <!-- ======= SOLUCIONES ======= -->
    @include('layouts.utils.services')
    <!-- ======= FIN SOLUCIONES ======= -->
    
    <!-- ======= INFO OFFICE ======= -->
    <section id="contact" class="contact">
        <div class="container">
            <div class="section-title">
                <h2>Info Office</h2>
            </div>
        </div>

        <div class="container">
            <div class="row mt-5">
            <div class="col-lg-4">
                <div class="info">
                    <div class="address">
                        <i class="bi bi-geo-alt"></i>
                        <h4>Ubicación:</h4>
                        <p>Bogotá, Colombia.</p>
                    </div>
                    <div class="email">
                        <i class="bi bi-envelope"></i>
                        <h4>Correo: </h4>
                        <p>soluciones@overallpharma.com</p>
                    </div>
                    <div class="phone">
                        <i class="bi bi-phone"></i>
                        <h4>Teléfono:</h4>
                        <p>(601) 2360042</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 mt-5 mt-lg-0">
                <iframe style="border:0; width: 100%; height: 500px;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d254508.3928070812!2d-74.24789584881651!3d4.648625942144889!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8e3f9bfd2da6cb29%3A0x239d635520a33914!2zQm9nb3TDoQ!5e0!3m2!1ses!2sco!4v1641450943658!5m2!1ses!2sco" frameborder="0" allowfullscreen></iframe>
              </div>
        </div>
    </section>
     <!-- ======= FIN INFO OFFICE ======= -->

  </main>

  @include('layouts.components.footer')

  @include('layouts.components.end')