
<div id="topbar" class="d-flex align-items-center fixed-top">
    <div class="container d-flex justify-content-between">
      <div class="contact-info d-flex align-items-center">
        <i class="bi bi-envelope"></i> <a href="mailto:contact@example.com">soluciones@overallpharma.com</a>
        <i class="bi bi-phone"></i> (601) 2360042
      </div>
      @if (Session::has('create'))
      <div class="contact-info d-flex align-items-center">
        <label for="" class="lead" style="color: #45B9B5; font-weight: bold"><i class="bi bi-check2-circle" style="color: #45B9B5;"></i> {{ Session::get('create') }}</label>  
      </div>
      @endif
    </div>
  </div>


  <header id="header" class="fixed-top">
    <div class="container d-flex align-items-center">
      <a href="{{ URL::to('/') }}" class="logo me-auto"><img src="{{url('img/logo_overall.png')}}" alt="" class="img-fluid"></a>

      <nav id="navbar" class="navbar order-last order-lg-0">
        <ul>
          <li><a class="nav-link scrollto active" href="{{ URL::to('/') }}">Inicio</a></li>
          <li class="dropdown"><a href="#"><span>Nosotros</span> <i class="bi bi-chevron-down"></i></a>
            <ul>
              <li><a href="{{route('about.index')}}#history">Historia</a></li>
              <li><a href="{{route('about.index')}}#team">Equipo Ejecutivo</a></li>
              <li><a href="{{route('about.index')}}#mission">Misión</a></li>
              <li><a href="{{route('about.index')}}#view">Visión</a></li>
              <li><a href="{{route('about.index')}}#ourclients">Nuestros Clientes</a></li>
            </ul>
          </li>
          <li><a class="nav-link scrollto" href="{{route('solution.index')}}">Servicios</a></li>
          <li><a class="nav-link scrollto" href="{{route('team.index')}}">IT</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav>
      <a href="javascript:;" class="appointment-btn scrollto btn-second" data-toggle="modal" data-target="#exampleModalCenter"><span class="d-none d-md-inline"><i class="bi bi-journal-text" ></i> Contáctenos</a>
  
    </div>
  </header>