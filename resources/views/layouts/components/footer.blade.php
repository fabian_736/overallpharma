
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row ">

          <div class="col-lg-3 col-md-6 footer-contact">
            <a href="{{ URL::to('/') }}" class="logo me-auto"><img src="{{url('img/logo_overall.png')}}" alt="" class="img-fluid w-25"></a>
         
          </div>

          <div class="col-lg-9 col-md-9 footer-links ">
            <h4>Menú</h4>
            <ul id="listas">
              <li><i class="bi bi-chevron-right"></i> <a href="{{ URL::to('/') }}">Inicio</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="{{route('about.index')}}">Nosotros</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="{{route('solution.index')}}">Servicios</a></li>
              <li><i class="bi bi-chevron-right"></i> <a href="{{route('team.index')}}">IT</a></li>
            </ul>
          </div>
        <style>
            #listas li{
                display: inline-block !important;
                margin-right: 5%;
            }
        </style>
        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="me-md-auto text-center text-md-start">
        <div class="copyright">
          &copy; Copyright © <script>
            document.write(new Date().getFullYear())
          </script>, All Rights Reserved
        </div>
      </div>
      
    </div>
  </footer>