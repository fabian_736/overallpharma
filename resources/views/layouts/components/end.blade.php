<div id="preloader"></div>
<a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

<!-- Template Main JS File -->
<script src="{{url('js/app.js')}}"></script>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>


<style>
  .btn-second{
      background: #ffffff;
      border-width: 2px;
      border-color: #197FA8;
      border-style: solid;
      color: #197FA8;
      font-weight: bold
  }

  body::-webkit-scrollbar {
  width: 10px;               /* width of the entire scrollbar */
}

body::-webkit-scrollbar-thumb {
  background-color: #197FA8;   /* color of the scroll thumb */
  border-radius: 20px;       /* roundness of the scroll thumb */
}
</style>

<style>
  @media (min-width: 1366px){
      #hero{
        width: 100%;
        height: 90vh;
        background: url("/img/hero-bg.jpg") top center;
        background-size: cover;
        margin-bottom: -100px;
      }

     
  }

  @media (min-width: 1920px){
      #hero{
        width: 100%;
        height: 80vh;
        background: url("/img/hero-bg.jpg") top center;
        background-size: cover;
        margin-bottom: -100px;
      }
  }
</style>
</body>

</html>