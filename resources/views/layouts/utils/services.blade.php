    <!-- ======= SOLUCIONES ======= -->
    <section id="services" class="services">
        <div class="container ">

            <div class="section-title ">
                <div class="row ">
                    <div class="col-5 d-flex justify-content-start align-items-center ">
                        <a href="javascript:;" onclick="window.location.reload();" style="display: none" class="back">
                            <i class="bi bi-arrow-return-left text-primary fa-lg" style="font-weight: bold;"></i> <span class="text-primary fa-lg mx-2" style="font-weight: bold;">VOLVER</span>
                        </a>
                    </div>
                    <div class="col d-flex justify-content-start">
                        <h2>Servicios</h2>
                    </div>
                </div>
                
                <p>Contamos con nuestros propios desarrollos digitales que combinados con la probada experiencia de
                    nuestro equipo de trabajo interdisciplinario garantizan la excelencia ejecucional de cada proyecto
                    que nos es confiado.
                </p>
            </div>

            <div class="row">

                <!-- SERVICIO 1 -->

                <div class="col-lg-6 col-md-6 d-flex align-items-stretch mb-5">
                    <a href="javascript:;" onclick="function_one()">
                        <div class="icon-box item1 col-12 " style="background: url('./img/services/trade2.jpg'); background-size: cover; height: 300px; cursor: pointer">
                            <div class="row ">
                                <div class="col d-flex justify-content-center align-items-center" style="margin-top: 10%">
                                    <h4><a href="javascript:;" onclick="function_one()">Programa de Soporte de Pacientes
                                        (PSPs)</a></h4>
                                </div>
                            </div>
                           
                        </div>
                    </a>
                </div>

                <div class="col-lg-12 col-md-12 d-flex align-items-stretch mb-3 carditem1"
                    style="display: none !important">
                    <div class="card shadow" style="border-radius: 20px">
                        <div class="row-reverse my-3">
                            <div class="col d-flex justify-content-center my-3">
                                <h4><a href="javascript:;">Programa de Soporte de Pacientes
                                        (PSPs)</a></h4>
                            </div>
                            <div class="col-10 d-flex justify-content-center mx-auto">
                                <p>
                                    En Overall Pharma sabemos que asegurar la <b>Adherencia</b> de los pacientes a los
                                    tratamientos prescriptos por el cuerpo médico de productos del portafolio de
                                    nuestros clientes, es un factor crítico para garantizar la mejor calidad de vida de
                                    los pacientes, el retorno de la inversión médica relacionado con la educación médica
                                    continuada y la disminución de costos del sistema de salud. <br><br>

                                    Para apoyar a las empresas de salud en el alcance de sus objetivos de
                                    <b>Adherencia</b>,
                                    contamos con una plataforma digital propia, <b>LivePatientCare</b>, omnicanal y
                                    omnimodal,
                                    que permite un proactivo y a la vez dinámico manejo de PSPs, pensando y actuando en
                                    todo momento en lograr nuestro objetivo esencial: <b>100% de Adherencia a
                                        tratamientos
                                        prescriptos.</b> <br><br>

                                    Nuestra solución tecnológica permite ajustes que son ejecutados por nuestro propio
                                    equipo de desarrollo que permiten cubrir necesidades específicas de nuestros
                                    clientes, asegurando el correcta y efectivo manejo de sus PSPs, el análisis de
                                    información de los mismos así cómo la integración y el entendimiento con softwares
                                    de nuestros contratantes. <br><br>

                                    Contamos con un equipo profesional de <b>TALENTO HUMANO</b> cuya metodología de
                                    reclutamiento asegura la atracción, contratación y retención de personal destinado
                                    al manejo de PSP´s que muestren altos standards profesionales, experiencia y calidad
                                    personal probada; buscando así, sistemáticamente cuidar y mantener en lo más alto la
                                    imágen de nuestros clientes frente a sus pacientes/ cuidadores, entidades de salud y
                                    cuerpo médico: nuestros contratados son sus mejores representantes. <br><br>

                                    <b>LivePatientCare</b> cuenta con un software aliado <b>LiveForceTraining</b>, que
                                    nos permite el
                                    entrenamiento y evaluación continua de los colaboradores contratados para el manejo
                                    de los PSP’s . Usamos los módulos de entrenamiento e integramos plataformas de
                                    nuestros clientes; podemos desarrollar los entrenamientos de ser necesario, con el
                                    apoyo de profesionales médicos que contratamos para asegurar la calidad del servicio
                                    prestado (en éste caso buscamos y aseguramos la aprobación del cliente previo a la
                                    difusión del entrenamiento). <br><br>

                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- FIN SERVICIO 1 -->


                <!-- SERVICIO 2 -->

                <div class="col-lg-6 col-md-6 d-flex align-items-stretch mb-5">
                    <a href="javascript:;" onclick="function_two()">
                        <div class="icon-box item2 col-12" style="background: url('./img/services/promocion2.jpg'); background-size: cover; cursor: pointer">
                            <div class="row ">
                                <div class="col d-flex justify-content-center align-items-center" style="margin-top: 10%">
                                    <h4><a href="javascript:;" onclick="function_two()">Promoción Médica</a></h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-12 col-md-12 d-flex align-items-stretch mb-3 carditem2"
                    style="display: none !important">
                    <div class="card shadow" style="border-radius: 20px">
                        <div class="row-reverse my-3">
                            <div class="col d-flex justify-content-center">
                                <div class="icon">
                                    <i class="bi bi-journal-bookmark-fill fa-2x"></i>
                                </div>
                            </div>
                            <div class="col d-flex justify-content-center my-3">
                                <h4><a href="javascript:;">Promoción Médica</a></h4>
                            </div>
                            <div class="col-10 d-flex justify-content-center mx-auto">
                                <p>
                                    Teniendo en cuenta la etapa del ciclo de vida que atraviesa una tecnología
                                    farmacéutica, desarrollamos soluciones de Visita Médica Presencial, Virtual y
                                    combinadas con el objetivo de maximizar el retorno de la inversión de nuestro
                                    cliente en el proceso de educación y recordación prescriptiva. <br><br>

                                    Contamos con una plataforma digital propia, <b>LiveMDProm</b> que asegura eficiencia
                                    promocional médica a través de la medición continua de parámetros de cumplimiento
                                    (KPI’s), con un proactivo seguimiento de targets y planes de visita. Nuestra
                                    solución tecnológica permite ajustes que son ejecutados por nuestro propio equipo de
                                    desarrollo, que permiten cubrir necesidades específicas de nuestros clientes,
                                    asegurando la correcta y efectiva promoción de su portafolio así cómo la
                                    conectividad con soluciones de los mismos. <br><br>

                                    Contamos con un equipo profesional de <b>Talento Humano</b>, cuya metodología de
                                    reclutamiento asegura la atractividad, contratación y retención de personal
                                    destinado a la promoción, que muestren altos estándares profesionales, experiencia y
                                    calidad personal probada; buscamos así sistemáticamente cuidar y mantener en lo más
                                    alto la imágen del portafolio de nuestros clientes frente al cuerpo médico: nuestros
                                    contratados son sus mejores representantes. <br><br>

                                    <b>LiveMDProm</b> cuenta con un software aliado <b>LiveForceTraining</b>, que nos
                                    permite
                                    entrenamiento y evaluación continua de la fuerza promocional y de ventas contratada.
                                    Usamos los módulos de entrenamiento e integramos plataformas de nuestros clientes;
                                    podemos desarrollar los entrenamientos de ser necesario, con el apoyo de
                                    profesionales médicos que contratamos para asegurar la calidad del servicio prestado
                                    (en éste caso buscamos y aseguramos la aprobación del cliente previo a la difusión
                                    del entrenamiento). <br><br>


                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- FIN SERVICIO 2 -->


                <!-- SERVICIO 3 -->

                <div class="col-lg-6 col-md-6 d-flex align-items-stretch mb-5">
                    <a href="javascript:;" onclick="function_three()">
                        <div class="icon-box item3 col-12" style="background: url('./img/services/psp3.png'); background-size: cover; height: 300px; cursor: pointer">
                            <div class="row ">
                                <div class="col d-flex justify-content-center align-items-center" style="margin-top: 10%">
                                    <h4><a href="javascript:;" onclick="function_three()">Trade Marketing Farmacéutico</a></h4>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="col-lg-12 col-md-12 d-flex align-items-stretch mb-3 carditem3"
                    style="display: none !important">
                    <div class="card shadow" style="border-radius: 20px">
                        <div class="row-reverse my-3">
                            <div class="col d-flex justify-content-center">
                                <div class="icon">
                                    <i class="bi bi-journal-bookmark-fill fa-2x"></i>
                                </div>
                            </div>
                            <div class="col d-flex justify-content-center my-3">
                                <h4><a href="javascript:;">Trade Marketing Farmacéutico</a></h4>
                            </div>
                            <div class="col-10 d-flex justify-content-center mx-auto">
                                <p> <b>Overall Pharma</b> comparte con sus clientes la preocupación por asegurar el
                                    mayor retorno de
                                    la inversión realizada en actividades de Promoción Médica, para posicionar sus
                                    productos
                                    en los hábitos prescriptivos del cuerpo médico o bien inversiones de publicidad
                                    masiva
                                    para productos de libre venta : lograr que los productos se encuentren distribuidos
                                    y
                                    con inventario adecuado en farmacias y droguerías es la mejor garantía de retorno de
                                    la
                                    inversión señalada. <br><br>

                                    Para apoyar la gestión de retorno de inversión promocional ponemos a disposición de
                                    nuestros clientes soluciones de <b>Trade Marketing Farmacéutico</b> que se apoyan en
                                    una
                                    plataforma digital propia, <b>LiveTrade</b>, que permite planear y ejecutar visitas
                                    en tiempo
                                    real de comprobación de inventarios y transferencias de pedidos farmacéuticos entre
                                    punto de venta y distribuidor autorizado por el cliente. <br><br>

                                    Nuestra solución tecnológica permite ajustes que son ejecutados por nuestro propio
                                    equipo de desarrollo que facilitan cubrir necesidades específicas de nuestros
                                    clientes
                                    asegurando el correcto y efectivo manejo los programas de Trade Marketing
                                    Farmacéutico,
                                    el análisis de información de los mismos así cómo la integración y conectividad con
                                    softwares de nuestros contratantes. <br><br>

                                    Contamos con un equipo profesional de <b>TALENTO HUMANO</b> cuya metodología de
                                    reclutamiento
                                    asegura la contratación, atracción y retención de personal destinado a la promoción
                                    que
                                    muestren altos standards profesionales, experiencia y calidad personal probada;
                                    buscamos así sistemáticamente cuidar y mantener en lo más alto la imágen de nuestros
                                    clientes frente a los puntos de venta y distribuidores: nuestros contratados son sus
                                    mejores representantes . <br><br>

                                    <b>LiveTrade</b> cuenta con un software aliado <b>LiveForceTraining</b>, que nos
                                    permite entrenamiento
                                    y evaluación continua de los colaboradores contratados para el manejo de los
                                    Programas
                                    de Trade Marketing Farmacéutico. Usamos los módulos de entrenamiento e integramos
                                    plataformas de nuestros clientes; podemos desarrollar los entrenamientos de ser
                                    necesario, con el apoyo de profesionales médicos que contratamos para asegurar la
                                    calidad del servicio prestado (en éste caso buscamos y aseguramos la aprobación del
                                    cliente previo a la difusión del entrenamiento). <br><br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- FIN SERVICIO 3 -->


                <!-- SERVICIO 4 -->

                <div class="col-lg-6 col-md-6 d-flex align-items-stretch mb-5">
                    <a href="javascript:;" onclick="function_four()">
                        <div class="icon-box item4 col-12" style="background: url('./img/services/farma2.jpg'); background-size: cover; cursor: pointer">
                            <div class="row ">
                                <div class="col d-flex justify-content-center align-items-center" style="margin-top: 10%">
                                    <h4><a href="javascript:;" onclick="function_four()">Farmacovigilancia Activa</a></h4>
                                </div>
                            </div>
                            
                        </div>
                    </a>
                </div>

                <div class="col-lg-12 col-md-12 d-flex align-items-stretch mb-3 carditem4"
                    style="display: none !important">
                    <div class="card shadow" style="border-radius: 20px">
                        <div class="row-reverse my-3">
                            <div class="col d-flex justify-content-center">
                                <div class="icon">
                                    <i class="bi bi-shield-check fa-2x"></i>
                                </div>
                            </div>
                            <div class="col d-flex justify-content-center my-3">
                                <h4><a href="javascript:;">Farmacovigilancia Activa</a></h4>
                            </div>
                            <div class="col-10 d-flex justify-content-center mx-auto">
                                <p> Cómo subproducto de los Programas de Soporte de Pacientes (PSP’s) , Overall Pharma
                                    presta servicios profesionales (hoy únicos en Colombia) que permiten realizar
                                    Farmacovigilancia Activa. <br><br>

                                    Este proceso es conducido por profesionales médicos con especialización en el área
                                    de epidemiologia, contratados por nuestra empresa; es diferente a los procesos
                                    standard de Farmacovigilancia Pasiva requeridos para el manejo normal de PSP’s .
                                    Permite a los clientes evaluar en forma proactiva, sistemática, bajo metodología de
                                    scripts y procesos globales de entendimiento y extracción de la información
                                    proveniente de los pacientes incluidos en el Programa de Pacientes de un determinado
                                    producto, eventos adversos de sus noveles productos una vez prescriptos por el
                                    cuerpo médico y utilizados por los pacientes. 
                                    Esta información es utilizada por EPS, IPS y el cuerpo médico para garantizar la
                                    seguridad y perfil prescriptivo del label farmacéutico registrado del producto. <br><br>

                                    Para nuestros clientes, contar con información de Farmacovigilancia Activa es una
                                    herramienta de información clínica médica, utilizada en procesos de Minimización de
                                    Riesgos Clínicos.

                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- FIN SERVICIO 4 -->



            </div>

        </div>
    </section>
    <!-- ======= FIN SOLUCIONES ======= -->


    <script>
        const function_one = () => {
            $('.item1').hide();
            $('.item2').hide();
            $('.item3').hide();
            $('.item4').hide();
            $('.carditem1').show();
            $('.carditem2').hide();
            $('.carditem3').hide();
            $('.carditem4').hide();
            $('.contact').hide();
            $('.back').show();
        }

        const function_two = () => {
            $('.item1').hide();
            $('.item2').hide();
            $('.item3').hide();
            $('.item4').hide();
            $('.carditem1').hide();
            $('.carditem2').show();
            $('.carditem3').hide();
            $('.carditem4').hide();
            $('.contact').hide();
            $('.back').show();
        }

        const function_three = () => {
            $('.item1').hide();
            $('.item2').hide();
            $('.item3').hide();
            $('.item4').hide();
            $('.carditem1').hide();
            $('.carditem2').hide();
            $('.carditem3').show();
            $('.carditem4').hide();
            $('.contact').hide();
            $('.back').show();
        }

        const function_four = () => {
            $('.item1').hide();
            $('.item2').hide();
            $('.item3').hide();
            $('.item4').hide();
            $('.carditem1').hide();
            $('.carditem2').hide();
            $('.carditem3').hide();
            $('.carditem4').show();
            $('.contact').hide();
            $('.back').show();
        }

        
    </script>
