      <!-- ======= MODAL FORMULARIO ======= -->
      <section id="why-us" class="why-us">
        <div class="container">

          <div class="modal fade bd-example-modal-lg" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
              <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
              <div class="modal-content" >
                  <div class="modal-header">
                  <h5 class="modal-title lead" style="font-weight: bold; color: #197FA8" id="exampleModalLongTitle" >Escribenos</h5>
                  <i class="bi bi-x-circle fa-lg text-primary" data-dismiss="modal" aria-label="Close" style="cursor: pointer"></i>
                  </div>
                  <div class="modal-body">
                      <form action="{{ route('inscription.store') }}" method="post">
                        @csrf
                          <div class="row-reverse">
                              <div class="col mb-3">
                                  <label for="" style="font-weight: bold">INFORMACIÓN PERSONAL</label>
                              </div>
                              <div class="col my-3">
                                  <div class="row">
                                      <div class="col">
                                          <input class="form-control input-border" name="name" type="text" placeholder="NOMBRE" required>
                                      </div>
                                      <div class="col">
                                          <input class="form-control input-border" name="last_name" type="text" placeholder="APELLIDO" required>
                                      </div>
                                  </div>
                              </div>
                              <div class="col">
                                  <input class="form-control input-border" name="business" type="text" placeholder="EMPRESA" required>
                              </div>
                          </div>
                          <div class="row-reverse my-5">
                              <div class="col mb-3">
                                  <label for="" style="font-weight: bold">CONTACTO</label>
                              </div>
                              <div class="col">
                                  <div class="col">
                                      <input class="form-control input-border" name="email" type="email" placeholder="E-MAIL" required>
                                  </div>
                              </div>
                              <div class="col my-3">
                                  <div class="row">
                                      <div class="col">
                                          <input class="form-control input-border" name="phone" type="number" placeholder="TELÉFONO" required>
                                      </div>
                                      <div class="col">
                                          <select name="country" id="" class="form-control input-border" required>
                                              <option value="" selected disabled>PAÍS/REGIÓN</option>
                                              <option value="Argentina">Argentina</option>
                                              <option value="Bolivia">Bolivia</option>
                                              <option value="Brasil">Brasil</option>
                                              <option value="Chile">Chile</option>
                                              <option value="Colombia">Colombia</option>
                                              <option value="Costa Rica">Costa Rica</option>
                                              <option value="Cuba">Cuba</option>
                                              <option value="Ecuador">Ecuador</option>
                                              <option value="El Salvador">El Salvador</option>
                                              <option value="Guayana Francesa">Guayana Francesa</option>
                                              <option value="Granada">Granada</option>
                                              <option value="Guatemala">Guatemala</option>
                                              <option value="Guayana">Guayana</option>
                                              <option value="Haití">Haití</option>
                                              <option value="Honduras">Honduras</option>
                                              <option value="Jamaica">Jamaica</option>
                                              <option value="México">México</option>
                                              <option value="Nicaragua">Nicaragua</option>
                                              <option value="Paraguay">Paraguay</option>
                                              <option value="Panamá">Panamá</option>
                                              <option value="Perú">Perú</option>
                                              <option value="Puerto Rico">Puerto Rico</option>
                                              <option value="República Dominicana">República Dominicana</option>
                                              <option value="Surinam">Surinam</option>
                                              <option value="Uruguay">Uruguay</option>
                                              <option value="Venezuela">Venezuela</option>
                                          </select>
                                      </div>
                                  </div>
                              </div>
                          </div>
                          <div class="row-reverse my-5">
                              <div class="col mb-3">
                                  <label for="" style="font-weight: bold">DETALLES</label>
                              </div>
                              <div class="col">
                                  <textarea name="description" id="" placeholder="DESCRIPCIÓN" cols="30" rows="5" class="form-control input-border p-3"></textarea>
                              </div>
                              <div class="form-check my-3">
                                  <input type="checkbox" class="form-check-input" name="status" id="exampleCheck1">
                                  <label class="form-check-label" for="exampleCheck1">Autorizo a recibir información y comunicaciones por parte de Overall Pharma en mi correo electrónico</label>
                              </div>
                          </div>
                          <div class="row my-3">
                              <div class="col-8 mx-auto">
                                  <button type="submit" class="btn form-control btn-modal">ENVIAR</button>
                              </div>
                          </div>
                          <style>
                              .btn-modal{
                                background: #ffffff;
                                border-width: 2px;
                                border-color: #197FA8;
                                border-style: solid;
                                color: #197FA8;
                                font-weight: bold
                            }
                            .btn-modal:hover{
                                background: #197FA8;
                                border-width: 2px;
                                border-color: #197FA8;
                                border-style: solid;
                                color: #ffffff;
                                font-weight: bold
                            }
                          </style>
                        </form>
                          <div class="row my-3">
                              <div class="col">
                              <p class="text-center"> 
                                  <i>Con el envío de esta solicitud, usted acepta nuestros términos de uso y la política de privacidad. </i> 
                              </p>
                              </div>
                          </div>
                      
                  </div>
              </div>
              </div>
          </div>
  
          <style>
              .input-border{
                  border-radius: 20px;
              }
          </style>
        </div>
      </section>
      <!-- ======= FIN MODAL FORMULARIO ======= -->