@include('layouts.components.head')

@include('layouts.components.navbar')


  <main id="main">

    <!-- ======= MODAL FORMULARIO ======= -->
    @include('layouts.utils.modal')
    <!-- ======= FIN MODAL FORMULARIO ======= -->

    <!-- ======= CONTENT YIELD ======= -->
    @yield('content')
    <!-- ======= END CONTENT YIELD ======= -->

  </main>

  @include('layouts.components.footer')

  @include('layouts.components.end')