@extends('layouts.app')
@section('content')


    <!-- ======= About Section ======= -->
    <section id="about" class="about">
        <div class="container-fluid">

            <div class="row" id="history">
                <div
                    class="col-xl-5 col-lg-6 video-box1 d-flex justify-content-center align-items-stretch position-relative">
                    <a href="https://youtu.be/RD09DlXIMIs" class="glightbox play-btn mb-4"></a>
                </div>
                <style>
                    .about .video-box1 {
                        background: url("img/about/historia.jpeg") center center no-repeat !important;
                        background-size: cover !important;
                        min-height: 500px !important;
                    }

                </style>
                <div
                    class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                    <h3>¿Quiénes somos?</h3><br>
                        Overall Pharma es una compañía innovadora, soportada en tecnologías digitales de última generación,
                        enfocada en soluciones para el sector de salud, que hace parte del holding conformado por dos
                        empresas que han operado exitosamente en los mercados de Perú y Colombia: Overall Strategy y People
                        Marketing.
                    </p>

                    <div class="icon-box">
                        <div class="icon itemabout p-1">
                          <img src="{{url('img/logo/overall.png')}}" alt="" class="w-100">
                        </div>
                       
                        <h4 class="title"><a href="">Overall Strategy (1988)</a></h4>
                        <p class="description">Compañía Peruana, enfocada en el suministro de personal (BPO), para diferentes sectores de la economía, incluyendo el área de salud, aplicando soluciones estratégicas y operativas que permiten mejorar la operación de sus más de 250 clientes por más de 33 años.</p>
                    </div>

                    <div class="icon-box">
                        <div class="icon itemabout p-1">
                          <img src="{{url('img/logo/people.png')}}" alt="" class="w-100">
                        </div>
                        <h4 class="title"><a href="">People Marketing (1997)</a></h4>
                        <p class="description">Compañía colombiana con 25 años de trayectoria en el mercado local y regional latinoamericano, dedicada al Marketing Integral. Desde el año 2003, inicia una unidad de negocio en el sector salud, apoyando a la industria farmacéutica, prestadores y aseguradores de Salud.</p>
                    </div>

                    <div class="icon-box">
                        <div class="icon itemabout p-2">
                          <img src="{{url('img/logo/overall&people.png')}}" alt="" class="w-100">
                        </div>
                        <h4 class="title"><a href="">Holding Overall Strategy – People Marketing (2019)</a></h4>
                        <p class="description">En el año 2019, Overall Strategy inicia su proceso de internacionalización y compra acciones de People Marketing, trasladando todo su Know How y sus plataformas relacionadas con los procesos de talento humano,  dando origen a People by Overall</p>
                    </div>

                    <div class="icon-box">
                        <div class="icon itemabout p-3">
                          <img src="{{url('img/logo_overall2.png')}}" alt="" class="w-100">
                        </div>
                        <h4 class="title"><a href="">Creación de Overall Pharma (2021)</a></h4>
                        <p class="description">En el año 2021, decidimos crear una nueva compañía colombiana, totalmente enfocada en el sector salud.</p>
                    </div>

                </div>
            </div>

             <!-- ======= Doctors Section ======= -->
  <section id="team" class="doctors">
    <div class="container">

      <div class="section-title">
        <h2>Equipo Ejecutivo</h2>
        
      </div>

      <div class="row ">

        <div class="col-lg-3 card card-body shadow">
         <div class="row">
           <div class="col ">
             <img src="{{url('img/doctors/circle-1.png')}}" alt="" class="w-100">
           </div>
           <div class="col">
             <div class="row-reverse">
                <div class="col ">
                  <label for="" class="h4">Juan Manuel Casas</label>
                </div> <hr>
                <div class="col ">
                  <p>Gerente General</p>
                </div>
             </div>
           </div>
         </div>
        </div>

        <div class="col-lg-3 card card-body shadow  mx-3">
          <div class="row">
            <div class="col ">
              <img src="{{url('img/doctors/circle-2.png')}}" alt="" class="w-100">
            </div>
            <div class="col d">
              <div class="row-reverse">
                 <div class="col ">
                   <label for="" class="h4">Fabiola Garcia</label>
                 </div> <hr>
                 <div class="col ">
                   <p>Gerente de Operaciones Farmacéuticas</p>
                 </div>
              </div>
            </div>
          </div>
         </div>

         <div class="col-lg-3 card card-body shadow">
          <div class="row">
            <div class="col ">
              <img src="{{url('img/doctors/circle-3.png')}}" alt="" class="w-100">
            </div>
            <div class="col d">
              <div class="row-reverse">
                 <div class="col ">
                   <label for="" class="h4">Carlos Mesia</label>
                 </div> <hr>
                 <div class="col ">
                   <p>Director IT</p>
                 </div>
              </div>
            </div>
          </div>
         </div>

      
      </div>

      <div class="row my-5">

        <div class="col-lg-3 card card-body shadow ">
          <div class="row">
            <div class="col ">
              <img src="{{url('img/doctors/circle-4.png')}}" alt="" class="w-100">
            </div>
            <div class="col">
              <div class="row-reverse">
                 <div class="col ">
                   <label for="" class="h4">Alejandro Martín</label>
                 </div> <hr>
                 <div class="col description-scroll" style="overflow: auto; height: 100px;">
                   <p>Ingeniero Industrial, MBA. Más de 30 años en la industria farmacéutica y OPTC en puestos ejecutivos y de dirección general en Latinoamérica. Jefe de una gran empresa farmacéutica, responsable del éxito de los lanzamientos de tecnologías innovadoras, y del desarrollo/despliegue de una relación sostenible y constructiva entre el gobierno y los agentes sanitarios. Fue miembro de la Asociación Colombiana de la Industria Farmacéutica (2007 - 2015) y miembro de la junta directiva de Fifarma (Asociación Latina de Empresas Farmacéuticas de R&D).</p>
                 </div>

                <style>
                    .description-scroll::-webkit-scrollbar {
                      width: 10px;               /* width of the entire scrollbar */
                    }

                    .description-scroll::-webkit-scrollbar-thumb {
                      background-color: #197FA8;   /* color of the scroll thumb */
                      border-radius: 20px;       /* roundness of the scroll thumb */
                    }
                </style>
              </div>
            </div>
          </div>
         </div>

         <div class="col-lg-3 card card-body shadow mx-3">
          <div class="row">
            <div class="col ">
              <img src="{{url('img/doctors/circle-5.png')}}" alt="" class="w-100">
            </div>
            <div class="col d">
              <div class="row-reverse">
                 <div class="col ">
                   <label for="" class="h4">Efrain Meneses</label>
                 </div> <hr>
                 <div class="col ">
                   <p>Médico General Magister</p>
                 </div>
              </div>
            </div>
          </div>
         </div>

         <div class="col-lg-3 card card-body shadow">
          <div class="row">
            <div class="col ">
              <img src="{{url('img/doctors/circle-6.png')}}" alt="" class="w-100">
            </div>
            <div class="col d">
              <div class="row-reverse">
                 <div class="col ">
                   <label for="" class="h4">Cesar Gómez</label>
                 </div> <hr>
                 <div class="col description-scroll" style="overflow: auto; height: 100px;">
                  <p>Administrador de Empresas con especialización en Marketing Estratégico y certificación del Programa de Desarrollo Gerencial. Más de 25 años de experiencia en Marketing, Ventas y Promoción Médica en empresas farmacéuticas. Conocimientos y habilidades en técnica de ventas y gestión eficaz de equipos comerciales. Desarrollo de planes comerciales exitosos.</p>
                </div>
              </div>
            </div>
          </div>
         </div>

        

       
      
      </div>

    </div>
  </section><!-- End Doctors Section -->

            <div class="row " style="margin-top: 2%; margin-bottom: 5%;" id="mission">
                <div
                    class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                    <h3>Misión</h3>
                    <p>Somos una empresa que maximiza el potencial de desarrollo de negocios de nuestros clientes en el
                        ámbito del sector salud, analizando, estructurando y ejecutando soluciones a la medida, que
                        garanticen excelencia ejecucional para procesos en salud relacionados con la adherencia de pacientes
                        a tratamientos médicos prescriptos, farmacovigilancia activa, visita médica (virtual y física),
                        trade marketing farmacéutico e investigación de mercado; operamos bajo estrictos estándares de
                        compliance, colaborando así en el objetivo médico clínico y social de mejorar la calidad de vida de
                        los pacientes.</p>
                </div>
                <div
                    class="col-xl-5 col-lg-6 video-box2 d-flex justify-content-center align-items-stretch position-relative">
                    <a href="https://youtu.be/RD09DlXIMIs" class="glightbox play-btn mb-4"></a>
                </div>
                <style>
                    .about .video-box2 {
                        background: url("img/about/vision.jpeg") center center no-repeat !important;
                        background-size: cover !important;
                        min-height: 500px !important;
                    }

                </style>
            </div>

            <div class="row" id="view">
                <div
                    class="col-xl-5 col-lg-6 video-box3 d-flex justify-content-center align-items-stretch position-relative">
                    <a href="https://youtu.be/RD09DlXIMIs" class="glightbox play-btn mb-4"></a>
                </div>
                <style>
                    .about .video-box3 {
                        background: url("img/about/mision.jpeg") center center no-repeat !important;
                        background-size: cover !important;
                        min-height: 500px !important;
                    }

                </style>
                <div
                    class="col-xl-7 col-lg-6 icon-boxes d-flex flex-column align-items-stretch justify-content-center py-5 px-lg-5">
                    <h3>Visión</h3>
                    <p>Para el año 2025 ser la firma referente en Latinoamérica entre las empresas locales y extranjeras que brindan servicios de outsourcing a empresas del sector salud en actividades relacionadas con el aseguramiento de adherencia de pacientes a tratamientos médicos prescriptos, excelencia en ventas farmacéuticas, trade marketing farmacéutico y educación médica continuada.</p>

                </div>
            </div>

            </div>

        </div>
    </section><!-- End About Section -->

        <!-- ======= Testimonials Section ======= -->
        <section id="ourclients" class="testimonials">
          
          <div class="container">
    
            <div class="section-title ">
              <div class="section-title">
                <h2>Nuestros Clientes</h2>
              </div>
          </div>

            <div class="testimonials-slider swiper" data-aos="fade-up" data-aos-delay="100">
              @include('layouts.utils.carrousel.active')
              @include('layouts.utils.carrousel.inactive')
            </div>
    
          </div>
        </section><!-- End Testimonials Section -->
        

    <style>

      .about .icon-box:hover .itemabout {
        background: none !important;
        cursor: pointer;
      }

    
    </style>





@endsection
